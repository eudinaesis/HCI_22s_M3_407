import React from 'react';

import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StyleSheet, Text, View, Pressable, Button,TouchableOpacity } from 'react-native';
import { mainContainerStyles as styles } from './config/styles';

// price context
import { PriceProvider } from './utils/priceContext';

// screens
import HomeScreen from './screens/HomeScreen';
import TradingScreen from './screens/TradingScreen';
import PortfolioScreen from './screens/PortfolioScreen';
import TradingHistoryScreen from './screens/TradingHistoryScreen';
import ConfigureWalletsScreen from './screens/ConfigureWalletsScreen';
import WalletScreen from './screens/WalletScreen';
import TradingConfirmationScreen from './screens/TradingConfirmationScreen';
import SettingsScreen from './screens/SettingsScreen.js'
import ConfigurePaymentScreen from './screens/ConfigurePaymentScreen';
import colors from './config/colors';
const homeName = 'HomeStack';
const tradingName = 'Buy & Sell';
const portfolioName = 'Portfolio';
const tradingHistoryName = 'TradingHistory';
const configureWalletName = "Configure Wallets";
const Tab = createBottomTabNavigator();

const HomeStack = createNativeStackNavigator();
function HomeStackScreen({navigation}) {
    return (
      <HomeStack.Navigator
      >
        <HomeStack.Screen name={"Home"} component={HomeScreen} options={{headerStyle:{backgroundColor:colors.secondary}}}/>
        <HomeStack.Screen name={"Settings"} component={SettingsScreen} options={{headerStyle:{backgroundColor:colors.secondary}}}/>
        <HomeStack.Screen name={"Configure Payment Options"} component={ConfigurePaymentScreen} options={{headerBackgroundColor:colors.secondary}}/>
       
      </HomeStack.Navigator>
    );
  }
const PortfolioStack = createNativeStackNavigator();
  function PortfolioStackScreen({navigation}) {
    return (
      <PortfolioStack.Navigator
      >
        <PortfolioStack.Screen name={"Overview"} component={PortfolioScreen} options={{headerTitle:'Portfolio',headerStyle:{backgroundColor:colors.secondary}}}/>
        <PortfolioStack.Screen name={configureWalletName} component={ConfigureWalletsScreen} options={{headerStyle:{backgroundColor:colors.secondary}}}/>
        <PortfolioStack.Screen name={"WalletScreen"} component={WalletScreen} options={({ route }) => ({ title: route.params.name, headerStyle:{backgroundColor:colors.secondary}})} />
      </PortfolioStack.Navigator>
    );
  } 

  const TradingStack = createNativeStackNavigator();

  function TradingStackScreen({}) {
      return (
        <TradingStack.Navigator
        >
          <TradingStack.Screen name={"TradingScreen"} component={TradingScreen} initialParams={{buyRoute: undefined, sellRoute: 'EUR'}}  options={{headerTitle:'Trade',headerStyle:{backgroundColor:colors.secondary}}}/>
          <TradingStack.Screen name={"TradingConfirmationScreen"} component={TradingConfirmationScreen}  options={{headerTitle:'Confirm', headerStyle:{backgroundColor:colors.secondary}}}/>
        </TradingStack.Navigator>
      );
    }


export default function MainContainer({navigation}){
    return(
        <PriceProvider>
            <NavigationContainer>
                <Tab.Navigator
                    initialRouteName={homeName}
                    screenOptions={({ route }) => ({
                        headerShown : false,
                        tabBarIcon: ({ focused, color, size }) => {
                            let iconName;
                            let rn = route.name;
                            if (rn === homeName) {
                                iconName = focused ? 'home' : 'home-outline';
                            } else if (rn === tradingName) {
                                iconName = focused ? 'repeat' : 'repeat-outline'; //'swap-horizontal' : 'swap-horizontal-outline'//'rocket' : 'rocket-outline'
                            } else if (rn === portfolioName) {
                                iconName = focused ? 'analytics' : 'analytics-outline'; //'trending-up' : 'trending-up-outline'
                            } else if (rn === tradingHistoryName) {
                                iconName = focused ? 'list' : 'list-outline';
                            }
                            return <Ionicons name={iconName} size={size} color={color} />;
                        },
                        tabBarActiveTintColor: styles.tabBarActiveTintColor,
                        tabBarInactiveTintColor: styles.tabBarInactiveTintColor,
                        tabBarLabelStyle: styles.tabBarLabelStyle,
                        tabBarActiveBackgroundColor: colors.secondary,
                        tabBarInactiveBackgroundColor: colors.white
                    })}
                >
                    <Tab.Screen name={homeName} component={HomeStackScreen} />

                    <Tab.Screen name={tradingName} component={TradingStackScreen} options={{headerBackgroundColor:colors.secondary}}/>
                    <Tab.Screen name={portfolioName} component={PortfolioStackScreen} options={{headerBackgroundColor:colors.secondary}}/>                   
                    <Tab.Screen name={tradingHistoryName} component={TradingHistoryScreen} options={{headerBackgroundColor:colors.secondary}}/>
                </Tab.Navigator>
            </NavigationContainer>
        </PriceProvider>
    )
}