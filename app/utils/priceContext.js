import React, {useState, useContext, useReducer} from 'react';
import Storage from './storage';
import initialPriceData from '../config/initialPriceData';

// pattern from https://www.robinwieruch.de/react-usecontext-hook/

const storage = new Storage;

const PriceContext = React.createContext();

function reducer(state, action) {
  switch (action.type) {
    case "overwrite":
      return action.value;
    case "updateOnePrice":
      const key = action.value.key;
      const newPrice = action.value.price;
      const newPrices = {...state};
      newPrices[key]["currentPrice"] = newPrice;
      return newPrices;
    default:
      throw new Error(`${action.type} is not a valid action!`);
  }
}

const PriceProvider = ({ children }) => {
//  const [prices, setPrices] = useState(initialPriceData);
  const [prices, setPrices] = useReducer(reducer, initialPriceData );
  console.log("From price provider: ");
  for (const key in prices) {
    console.log(`${key}: ${prices[key]["currentPrice"]}`);
  }

  return (
    <PriceContext.Provider value={[prices, setPrices]}>
      {children}
    </PriceContext.Provider>
  );
};

export { PriceContext, PriceProvider };