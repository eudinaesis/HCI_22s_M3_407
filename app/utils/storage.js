import * as SecureStore from 'expo-secure-store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Currencies from '../config/currencyData';
//expo install @react-native-async-storage/async-storage
//expo install expo-secure-store
import initialPriceData from '../config/initialPriceData';
import {DevSettings} from 'react-native';


// wallets use SecureStore
// transaction history uses AsyncStorage
// prices use AsyncStorage

export default class Storage {
    constructor(){
        //AsyncStorage.clear()
        //SecureStore.deleteItemAsync("wallets")
    }

    async loadPrices(){ // return prices{} promise
        console.log("loading prices... ");
        AsyncStorage.getItem("prices")
            .then( (result) => {
                return (result != null ? JSON.parse(result) : initialPriceData);
            })
            .catch( (err) => { 
                console.log(err);
                return {};
            });
    }

    async createWallet(key, amount = 0){ //create and store new wallet/currency 

        let wallet = {  
            key : key,
            amount : amount,
        }

        this.loadWallets()
        .then((wallets=> {
            if(wallets){
                if(!wallets.some(w=> w.key ==wallet.key)) //prevent double
                    wallets.push(wallet)
            }
            else   
                wallets = [wallet]
            SecureStore.setItemAsync("wallets",JSON.stringify(wallets))
        }))
    }
    

    async logAction(key,amount,date,incoming){ //add action to history return loggedAction
        let action = {
            key : key,
            amount : amount,
            date : date,
            incoming :incoming,
        }
        
        this.getHistory().then(history=>{
            if(history){
                history.push(action)
            }else{
                history = [action]
            }
            AsyncStorage.setItem("history",JSON.stringify(history))
        })
        
        return action
    }
    
    async updateWallet(key,newAmount){//set newAmount return wallet
        let wallet = this.loadWallets().then(wallets=>{
            if(wallets){
                let wallet = wallets.find(wallet=> wallet.key ==key)
                console.log("Update wallet old wallet: "+wallet)
                wallet.amount = newAmount
                console.log("Update wallet new wallet: "+JSON.stringify(wallets))
                SecureStore.setItemAsync("wallets",JSON.stringify(wallets))
                return wallet
            }else {console.log("storage: updateWallet called but no wallets were found")}
        })
        return wallet
    }
    async loadWallets(){ //return wallet[] - promise 
        console.log("loading wallets")
        let wallets = await SecureStore.getItemAsync("wallets")
        .then(result=>{
            let wallets = JSON.parse(result)
            return wallets
        })
        console.log(JSON.stringify(wallets))
        return wallets
        
    }

    async getHistory(){ //return action[] - promise
        let history = await AsyncStorage.getItem("history")
        .then(result=>{
            let history = JSON.parse(result)
            return history
        })
        if(history)
            return history
        return []
    }

  
    async removeWallet(key){
        this.loadWallets().then(wallets=>{
            if(wallets){
                var index = wallets.findIndex(w=> w.key==key)
                if(!wallets[index].amount==='0')
                    return false
                wallets.splice(index,1) 
                SecureStore.setItemAsync("wallets",JSON.stringify(wallets))
                
                return true
            }else {return false}
        })
        }
    
    async saveSettings(settingsObj){
        AsyncStorage.setItem("settings",JSON.stringify(settingsObj))
    }

    async setTutorialDone(type){
        AsyncStorage.setItem(type,JSON.stringify(false))
    }
    async getTutorial(type){
        return AsyncStorage.getItem(type).then(res=>{
            var result = JSON.parse(res)
            if(result==undefined)
                return true
            return res=='true' ? true:false
            
        }
        )
    }
    async resetTutorial(){
        AsyncStorage.setItem("firstTimeTrade",JSON.stringify(true))
        AsyncStorage.setItem("firstTimeHome",JSON.stringify(true))
        AsyncStorage.setItem("firstTimeHistory",JSON.stringify(true))
        AsyncStorage.setItem("firstTimePortfolio",JSON.stringify(true))
        AsyncStorage.setItem("firstTimeTradingConfirmation",JSON.stringify(true))
    }
      
      

    async loadSettings(){
        return AsyncStorage.getItem("settings").then(set=>{
            settings=JSON.parse(set)
            if(!settings){
                    settings = {
                    payment : undefined,
                }
               
            } 
            return settings
        }

        )

    }
    async resetApp(){
        AsyncStorage.clear()
        SecureStore.deleteItemAsync("wallets")
        resetTutorial()
    }
}