import React ,{ useState, useContext }from 'react';
import {Button, Text, View,Image, StyleSheet } from 'react-native';
import { homeScreenStyles as styles } from '../config/styles';
import Storage from '../utils/storage';
import ChartComponent from '../components/ChartComponent';
import { useFocusEffect } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { PriceContext } from '../utils/priceContext';

const dollarSymbol = '$';
const eurSymbol = '€';

export default function HomeScreen({ navigation }){
    const [wallets,setWallets] = useState();
    const [Tutorial,setTutorial] = useState(false);
    const [totalValue,setTotalValue] = useState(0);
    const storage = new Storage();
    const rateFromApi = 100

    const prices = useContext(PriceContext)[0];
    const setPrices = useContext(PriceContext)[1];

    useFocusEffect( //update when screen is focused
    React.useCallback(() => {
        storage.loadWallets().then(wallets=> setWallets(wallets))
        storage.getTutorial("firstTimeHome").then(set=>{
                    setTutorial(set)
                    storage.setTutorialDone("firstTimeHome")
                }
                )
    }, []))

    

    React.useEffect(() => {
       // console.log("from homescreen useEffect: ", prices["BTC"]["currentPrice"]);
        if(wallets){
            let amount = 0;
            wallets.forEach(wallet=> {
                let coinKey = wallet.key;
                let coinAmount = wallet.amount;
               // console.log("key: ", coinKey, "; amount: ", coinAmount);
                let coinPrice = prices[coinKey]["currentPrice"];
                amount += coinAmount * coinPrice;
               // console.log("total: " + amount);
        });
            setTotalValue(amount);
        }
      },[wallets, prices]);

//      React.useEffect(() => {
//        const interval = setInterval(() => {
//          console.log('This will run every 5 seconds and change btc price to random');
//          setPrices({ type: "updateOnePrice", value: { key: "BTC", price: Math.floor(Math.random() * 30000)}});
//        }, 5000);
//        return () => clearInterval(interval);
//      }, []);

      const settingsButton=()=>{
          if(Tutorial){
              //setSettings(settings.firstTimeHome=false)
              
              return (
                  
                <View style={styles.viewSetting}>  
                <Ionicons name='settings-outline' size={50} color='red'
                    onPress={()=>navigation.navigate("Settings")}/>
                <Text  style={styles.firsTimeTxt}>
                <Ionicons name='arrow-up-outline' size={30}/>
                    Click here to add a payment option
                </Text>
            </View>
           
              )
          }
          return( 
          <View style={styles.viewSetting}>  
            <Ionicons name='settings-outline' size={50} 
                onPress={()=>navigation.navigate("Settings")}/>
        </View>)
          
      }


    return(
     
    <View style={styles.viewTop}> 
        
       {settingsButton()}
        <View style={styles.viewContainer}>
            
                <Text style={styles.subtitle}>Total Value: {totalValue} {eurSymbol}   </Text>
                <ChartComponent purpose={'home'}/>
        </View>
    </View> 
    )
}

// <ChartComponent props = {{height:200}}/>
// funktioniert-braucht aber expo go restart {settings!=undefined ? settings.firstTime ? console.log("fist time"):console.log("not first time") : console.log("undefined")}