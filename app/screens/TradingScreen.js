import React, { useContext } from 'react';

import { useFocusEffect } from '@react-navigation/native';

//import Icon from 'react-native-ico-material-design';

import currencyData from '../config/currencyData';

import { Text, View, Switch, StyleSheet, TouchableOpacity, FlatList,Image,Button,ScrollView} from 'react-native';
import Storage from '../utils/storage';
import colors from '../config/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';

const usedCurrency='EUR';
import { tradingStyles as styles } from '../config/styles';

import { PriceContext } from '../utils/priceContext';

export default function TradingScreen({route, navigation }){
    const prices = useContext(PriceContext)[0];
//    console.log(prices)
//    const key = wallet.key;
//    console.log(key)
//    const price = prices[key]["currentPrice"];

    const storage = new Storage();
    const {buyRoute,sellRoute}=route.params;

    const [wallets, setWallets] = React.useState([]);
    const [isEnabled, setIsEnabled] = React.useState(false);
    const [buyDropdown, setBuyDropdownEnabled] = React.useState(false);
    const [sellDropdown, setSellDropdownEnabled] = React.useState(false);
    const[currentBuy,setBuy] = React.useState();//key of currency currently selected in buy
    const[currentSell,setSell] = React.useState();
    const[tutorial,setTutorial]=React.useState(false);
    useFocusEffect( //update when screen is focused
    React.useCallback(() => {
        storage.loadWallets().then(wallets=> {
            //wallets.push(currencyData.EUR);  
            setWallets(wallets); 
            if(buyRoute!=undefined)
                setBuy(buyRoute)
            else if(wallets!=null)
                setBuy(wallets[0].key); 
            if(sellRoute!=undefined)
                setSell(sellRoute)
            else
                setSell(currencyData[usedCurrency].key)
        })
        storage.getTutorial("firstTimeTrade").then(tutorial=>{
            setTutorial(tutorial)
           
            storage.setTutorialDone("firstTimeTrade")
        })
    }, [route]))

    

    const Dropdown = ({ action}) => {
        return (
            <View style ={styles.dropdown }>
                {wallets.concat(currencyData.EUR).map(wallet=> {
                              const data = currencyData[wallet.key]
                              if(data.key!=currentBuy&&data.key!=currentSell){
                                  return(
                                      
                                  <TouchableOpacity key={data.key}style={{padding:5, }}onPress={()=> action(data.key)}>
                                      <Text style={{fontSize:20}}>
                                      <Image style={{height:20,width:20}} source={{uri:data.icon}} />
                                        {data.name}
                                      </Text>
                                  </TouchableOpacity> 
                                  )
                              }
                })}
          
                    </View>
        );
            }
     

     
     
      

      const tradeButton=()=>{
        return(
            <View flex={1} alignItems={'center'}> 
            
                <TouchableOpacity style={styles.button}
                onPress={()=> navigation.navigate("TradingConfirmationScreen", {buyCur:currentBuy,sellCur:currentSell,wallets:wallets})}
                >
                    <Text>continue</Text>
                </TouchableOpacity>
               
            </View>
        )
      }

      const swapButton=()=>{
        return(
            <TouchableOpacity style={styles.swapButton}
            onPress={()=>{var oldSell = currentSell; setSell(currentBuy);setBuy(oldSell)}}
            >
                
                    <Ionicons name='swap-vertical-outline' size={30}/>
                   
            </TouchableOpacity>
        )
      }

      const toggleDropdown=(type)=>{
          if(type==currentBuy)
            setBuyDropdownEnabled(!buyDropdown)
        else if(type==currentSell)
            setSellDropdownEnabled(!sellDropdown)
            else{
                console.log("TradingScreen: toggleDropdown received wrong type")
            }
      }


      const dropdownButton=(type)=>{
          return(
            <TouchableOpacity style={styles.DropdownButton}
            onPress={()=>toggleDropdown(type)}>
                <Ionicons name='caret-down-outline' size={30}/>
            </TouchableOpacity>
          )
      }

    const renderSelectionPart=(type)=>{//currentBuy/currentSell
        if(type!=undefined)
        return(
            
            <View style={styles.innerView} >
                
                <Text style={styles.selectionPartText}>{type==currentBuy? 'Buy':'Pay with'}  {renderSelected(type)}</Text>
               
                  {dropdownButton(type)}
                {(type==currentBuy ? buyDropdown : sellDropdown) ? <Dropdown action={type==currentBuy ? setBuy:setSell}></Dropdown>: <></>}
               {tutorial ? <Text><Ionicons name='arrow-back-outline'/> Select currency you want to {type==currentBuy ? "Buy" : "Pay with"}</Text>: <></>}
            </View>
            
        )
        return null
    }

    const renderSelected = (selected)=>{
        return( 
        <>
        <Image style={styles.icons} source={{ uri: currencyData[selected].icon }}/>
        <Text > {currencyData[selected].key}</Text></>
        )
    }

    const renderAmountInfo=()=>{
        var wallet=wallets.find(w=> w.key==currentBuy); 
        
        return (
        <Text>You have {wallet.amount} {wallet.key}</Text>
        )
    }


    
    if(currentBuy!=null&&currentBuy!=undefined){
    return(
        
        <View style={styles.backgroundView}>
            {        console.log("trading screnn buy, sell route"+buyRoute,sellRoute)
}
        <ScrollView>
        <View style={styles.topContainer}> 
            
            {renderSelectionPart(currentBuy)}         

            {currentBuy!=usedCurrency ? renderAmountInfo() : <></>}
            
            <View style={styles.innerView}>
                
                
                {swapButton()}
                {tutorial ? 
                    <View>
                        <Text><Ionicons name='arrow-back-outline'/>click here to swap buy/sell </Text>
                        <Text>click here to enter amount<Ionicons name='arrow-forward-outline'/> </Text>
                    </View>
                    : <></>
                }
                
                {tradeButton()}
               
                
               
            </View>
           {renderSelectionPart(currentSell)}

            {currentSell!=usedCurrency ? <Text>You have {currentSell!=undefined ? wallets.find(w=> w.key==currentSell).amount : "0"}</Text> : <></>}
            
        

        </View>
        </ScrollView>
        </View>
    )
    }
    else{
        return (
            <View style={styles.backgroundView} alignItems={'center'}>
                
                <Text style={styles.beginnerHelpMessage}> 
                    You must add a wallet to your profile first
                </Text>
                <TouchableOpacity 
                   style={{
                    borderRadius: 10,
                    height: 50,
                    width: 100,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "lightgrey",
                    marginTop:50
                   }}
                    onPress={()=>navigation.navigate("Portfolio")}
                >
                    <Text>go to portfolio</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


 /*<View style={{alignItems:'center',justifyContent:'center',flex:1}}> 
            <View style={{alignItems:'center',justifyContent: 'center',flex:0.2,flexDirection:'row'}}>
                <Text style={{flex:0.2}}>{isEnabled ? "buy" : "sell"}</Text>
                <Switch
                    style={styles.switch}
                    trackColor={{false: 'red', true: 'green'}}
                    thumbColor={"#f4f3f4"}
                    onValueChange={toggleSwitch}
                    value={isEnabled}
                />
            </View>
            <View style={styles.viewButton}>
                <TouchableOpacity 
                    onPress={()=>{
                        alert('Success!')
                        storage.logAction(currency.BTC.key,'0.1',new Date(),isEnabled)//add to history array
                        storage.updateWallet(currency.BTC.key,'0.1')//add to portfolio array
                    }}
                    style={styles.btnTouchableOpacity}>
                    <Text style={styles.btnText}>{isEnabled ? "buy" : "sell"}</Text>
                </TouchableOpacity>
            </View>
        </View>
       
    )
}
 */