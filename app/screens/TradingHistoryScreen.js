import React from 'react';
import { Text, View, ScrollView, Button,FlatList } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

//import Icon from 'react-native-ico-material-design';
import colors from '../config/colors';
import TradingHistoryComponent from '../components/TradingHistoryComponent'
import currencyData from '../config/currencyData';
import Storage from '../utils/storage';

export default function TradingHistoryScreen({ navigation }){
    const storage = new Storage();

    const[data,setData] = React.useState([
        {key: currencyData.BTC.key, amount: '143,05€',date: '1.1.2000',incoming: true},
        {key: currencyData.BTC.key, amount: '143,05€',date: '2.1.2000',incoming: true},
        {key: currencyData.BTC.key, amount: '143,05€',date: '3.1.2000',incoming: false},
        {key: currencyData.BTC.key, amount: '143,05€',date: '4.1.2000',incoming: true},
        {key: currencyData.BTC.key, amount: '143,05€',date: '5.1.2000',incoming: false}
    ])
    
    function updateData() {
        storage.getHistory().then(history=> setData(history))
    }

    useFocusEffect( //update when screen is focused
       
        React.useCallback(() => {
            updateData()
        }, []))

    return(
             <View paddingTop={50} paddingBottom={50} backgroundColor={colors.white}>
                 <View alignItems={'center'}>
            <Text alignSelf={'center'}  style={{marginBottom: 5,fontSize:25,fontWeight:"bold"}}>
                Trading History
            </Text>
            </View>
            <ScrollView >
                {data.reverse().map((item,i) => {
                    return(
                        <TradingHistoryComponent key={i} data={item}/>
                    )
                })}
            </ScrollView>
        </View>
        
    )
}