
import React from 'react'
import { Text, View, Switch, StyleSheet, TouchableOpacity,FlatList,Button,ActivityIndicator} from 'react-native';
import colors from '../config/colors';
import { SettingsStyles as styles } from '../config/styles';
import Storage from '../utils/storage';

const button=(title,action)=>{
    return(
    <TouchableOpacity style={styles.button}
    onPress={()=>{action()}}
    >
        <Text style={styles.buttonTxt}>
            {title}
        </Text>
    </TouchableOpacity>
    )
}



export default function SettingsScreen({navigation}){ 

     return(
         <View style={styles.topView}>
             {button("Payment Options",()=>{navigation.navigate("Configure Payment Options")})}
             {button('Help',()=>{console.log("need some help!")})}
             {button('Reset Tutorial',()=>{new Storage().resetTutorial()})}
             {button('Dev reset App',()=>{new Storage().resetApp()})}
         </View>
     )

   
}
