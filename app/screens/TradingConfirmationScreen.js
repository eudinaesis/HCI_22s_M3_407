import React, {useState, useContext} from 'react'
import { Text, View, Switch, StyleSheet, TouchableOpacity, FlatList,Image,Button,TextInput,Alert,ScrollView} from 'react-native';
import { walletScreenStyles } from '../config/styles';
import Storage from '../utils/storage';
import colors from '../config/colors';
import currencyData from '../config/currencyData';
import { tradingConfirmationStyles as styles } from '../config/styles';
import { useFocusEffect } from '@react-navigation/native';

import { PriceContext } from '../utils/priceContext';

const usedCurrency = 'EUR';

export default function TradingConfirmationScreen ({route,navigation}){
    const prices = useContext(PriceContext)[0];
//    console.log("TradingConf: ", prices);
//    const key = wallet.key;
//    console.log(key)
//    const price = prices[key]["currentPrice"];

    const storage = new Storage();
    const{buyCur, sellCur,wallets} = route.params;//currency to buy, currency used to buy
    const[buyAmount,setBuyAmount] = useState(0); //amount in target currency
    const[sellAmount,setSellAmount] = useState(0); //amount in currency used to buy/trade
    const[monetaryValue,setMonetaryValue] = useState(0);  //amount of transaction in eur
    const[fees,setFees] = useState(2); //Network fees
    const[Tutorial,setTutorial]=useState(true);
    console.log("buying: ", buyCur, " and selling: ", sellCur);

    useFocusEffect( //update when screen is focused
        React.useCallback(() => {
            storage.getTutorial("firstTimeTradingConfirmation").then(res=>{
                setTutorial(res)
                //storage.setTutorialDone("firstTimeTradingConfirmation")
            })
    }, []))

    

    const inputInMoney=()=>{
        return(
            <>
           <Text style={styles.text}>Amount to buy/sell: </Text>
           <View style={styles.rowView}>
               <TextInput style = {styles.textInput}
                   onEndEditing={evt=>{ if(evt.nativeEvent.text != '')adjustAmounts(evt.nativeEvent.text,'money')}}
                   placeholder ={monetaryValue.toString()}
                   //value={monetaryValue.toString()}
               /> 
               <Text style={styles.text}>in EUR</Text>
           </View>
           </>
       )
    }

    const getEquivalentAmount=(fromCur,toCur,amount,knownVal)=>{//TODO: actual amount conversion
        console.log("gEA!!! buying: ", toCur, " and selling: ", fromCur);
        console.log(prices[toCur]["currentPrice"]);
        if (knownVal == toCur) {
            let targetPriceInEur = prices[toCur]["currentPrice"];
            let sellingPriceInEur = (fromCur == "EUR" ? 1 : prices[fromCur]["currentPrice"] );
            let priceInSellingUnits = (targetPriceInEur / sellingPriceInEur);
            let totalCost = amount * priceInSellingUnits;

            console.log("target price€: ", targetPriceInEur);
            console.log("sellingP €: ", sellingPriceInEur);
            console.log("unit price in selling units: ", priceInSellingUnits);
            console.log("Total cost of ", amount, " units: ", totalCost);
    //        return Number.parseFloat(amount)*10;
            return totalCost;
        } else if (knownVal == fromCur) {
            let targetPriceInEur = prices[toCur]["currentPrice"];
            let sellingPriceInEur = (fromCur == "EUR" ? 1 : prices[fromCur]["currentPrice"] );
            let priceInSellingUnits = (targetPriceInEur / sellingPriceInEur);
            let totalBuy = amount / priceInSellingUnits;

            console.log("target price€: ", targetPriceInEur);
            console.log("sellingP €: ", sellingPriceInEur);
            console.log("unit price in selling units: ", priceInSellingUnits);
            console.log("You can buy ", totalBuy, " units: ");
    //        return Number.parseFloat(amount)*10;
            return totalBuy;
            
        } else {
            return 10;
        }
   }

    const checkValidity=(tradedInAmount)=>{
         if(sellCur===usedCurrency||tradedInAmount<wallets.find(w=>w.key==sellCur).amount)
            return true;
        else{
             alert("Not enough "+sellCur+" to make this transaction")
         }
    }
    
    const isNumber=(string)=>{
        var reg = new RegExp("^([0-9]+\.?[0-9]*|\.[0-9]+)$")
        return reg.test(string)
    }

    const adjustAmounts=(entered, type)=>{
        if(isNumber(entered)){
            let tradedInAmount;
            switch(type){
                case 'buy':
                    tradedInAmount= getEquivalentAmount(sellCur,buyCur,entered,buyCur);
                    if(checkValidity(tradedInAmount)){//Amount to buy input was used
                    setBuyAmount(entered);
                    setSellAmount(tradedInAmount)
                    setMonetaryValue(getEquivalentAmount(usedCurrency,buyCur,entered,buyCur));
                    break;
                    }
                case 'sell':
                    if(checkValidity(entered)){//Amount to sell input was used
                    setSellAmount(entered);
                    setBuyAmount(getEquivalentAmount(sellCur,buyCur,entered,sellCur));
                    setMonetaryValue(getEquivalentAmount(usedCurrency,buyCur,buyAmount,buyCur));
                    break;
                    }
                case 'money': //money input was used
                    tradedInAmount= getEquivalentAmount(sellCur,usedCurrency,entered,usedCurrency);
                    if(checkValidity(tradedInAmount)){
                        setMonetaryValue(entered);
                        setBuyAmount(getEquivalentAmount(usedCurrency,buyCur,entered,usedCurrency));
                        setSellAmount(tradedInAmount);
                        break;
                    }

            
            }
        }else{
            alert("Enter valid number")
        }
    }
        const renderNetworkFees=()=>{//TODO actual network fees
            return(
                <View style={styles.networkFeeBox}>
                    <TouchableOpacity style={styles.networkFeeButton}
                    onPress={()=> setFees(1)}>
                        <Text>Low-Slow</Text>
                        
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.networkFeeButton}
                    onPress={()=> setFees(2)}>
                        <Text>
                            Medium
                        </Text>
                        
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.networkFeeButton}
                    onPress={()=> setFees(3)}
                    >
                        <Text>
                            High-Fast
                        </Text>
                        
                    </TouchableOpacity>
        
                </View>
            )
    }


    function renderTradeButton(){
        return(
            <TouchableOpacity
              style ={styles.tradeButton}
              onPress = {
                ()=>{ 
                    if(buyAmount>0)
                        showConfirmDialog()
                    else 
                        alert("Enter amount")

                }
              }
            >
            <Text>Trade</Text>
            </TouchableOpacity>
        )
    }

    const fullfillTransaction=()=>{//TODO payment in eur
        if(sellCur===usedCurrency){//bought with money
           alert("link to paypal opening")
        } else{//bought with crypto
            var soldWallet = wallets.find(w=> w.key===sellCur)
            var soldValue = Number.parseFloat(soldWallet.amount)-sellAmount;

            storage.updateWallet(sellCur,soldValue);
            storage.logAction(sellCur,sellAmount,new Date(),true)
        }
        if(buyCur===usedCurrency){//sold for money
            alert("link to paypal opening")
        }
       else{//sold for crypto
        var boughtWallet = wallets.find(w=> w.key===buyCur)
        var boughtValue = Number.parseFloat(boughtWallet.amount)+Number.parseFloat(buyAmount);
        storage.updateWallet(buyCur,boughtValue);
        storage.logAction(buyCur,buyAmount,new Date(),false);    
       }
    }

    const showConfirmDialog = () => {
        var text = "Buy "+buyAmount.toString()+" "+buyCur+" for "+sellAmount.toString()+" "+sellCur;
        return Alert.alert(
          "Confirm Trade",
          text,
          [
            {
              text: "Yes",
              onPress: () => {
                fullfillTransaction(); navigation.goBack()
              },
            },
            {
              text: "No",
            },
          ]
        );
      };

      const renderIntro=()=>{
          return(
              <View>              
                  <View style={styles.introView}>
                  
                <Text style={styles.text}>Buy {currencyData[buyCur].name}</Text> 
                <Text style={styles.text}> with {currencyData[sellCur].name}</Text> 
                
              </View>{Tutorial?renderTutorialTop():<></>}
              </View>

          )
      }
      const renderTutorialTop=()=>{
          return(
              <View>
                  <Text>
                      type the amount into one of the boxes-the others will be calculated automatically
                  </Text>
              </View>
          )
      }
      const renderTutorialBot=()=>{
        return(
            <View>
                <Text>
                    click here to view and confirm your trade
                </Text>
            </View>
        )
    }
    return(
        <View style ={styles.topView}>
        <ScrollView style={styles.scrollView}>
        <View style={styles.topView}>
            {renderIntro()}

            <Text style={styles.text}>Amount to buy: </Text>
            <View style={styles.rowView}>
                <TextInput style = {styles.textInput}
                    onEndEditing={evt=>{ if(evt.nativeEvent.text != '')adjustAmounts(evt.nativeEvent.text,'buy')}}
                    placeholder ={buyAmount.toString()}
                    keyboardType={'numeric'}
                    //value={buyAmount.toString()}
                /> 
                <Text style={styles.text}>in {buyCur}</Text>
            </View>

            <Text style={styles.text}>Amount to sell: </Text>
            <View style={styles.rowView}>
               
                <TextInput style = {styles.textInput}
                    
                    onEndEditing={evt=> {if(evt.nativeEvent.text != '')adjustAmounts(evt.nativeEvent.text, "sell")}}
                    //value={sellAmount.toString()}
                    placeholder ={sellAmount.toString()}
                    keyboardType='numeric'

                /> 
                <Text style={styles.text}>in {sellCur}</Text>
            </View>

            
            {buyCur==usedCurrency||sellCur==usedCurrency ? <></> : inputInMoney()}

            {renderTradeButton()}
            {Tutorial?renderTutorialBot():<></>}
            {sellCur!=usedCurrency ? 
                <View>
                    <Text style={styles.text}>Network fees: {fees} {sellCur}</Text>
                    { renderNetworkFees()}
                </View> 
                : <></>}
        </View>
        </ScrollView>
        </View>
    )
}



