import React , {useState} from 'react';
import { Text, View, Switch, StyleSheet, TouchableOpacity,FlatList, Confirm,Alert} from 'react-native';
//import Icon from 'react-native-ico-material-design';
import Storage from '../utils/storage';
import { useFocusEffect } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import WalletComponent from '../components/WalletComponent';
import ChartComponent from '../components/ChartComponent'
import Ionicons from 'react-native-vector-icons/Ionicons';
import currencyData from '../config/currencyData';
import { walletScreenStyles as styles } from '../config/styles';
import colors from '../config/colors';

const configureWalletName = "Configure Wallets";

const HomeStack = createNativeStackNavigator();
const plusIconSize = 50;

export default function WalletScreen({route, navigation }){
    const storage = new Storage();
    const { wallet } = route.params;



    function Button({title,buy}){
        var buyKey = 'EUR'
        var sellKey = 'EUR'
        buy ? buyKey = wallet.key : sellKey=wallet.key
        if(title==='Buy'){
          buyKey=wallet.key
        }
        else if(title==='Sell'){
          sellKey=wallet.key
        }
        return(
            <TouchableOpacity
              style ={styles.button}
              onPress = {()=>{navigation.navigate("Buy & Sell", {screen:'TradingScreen',params:{buyRoute:buyKey,sellRoute:sellKey }})}}
            >
            <Text>{title}</Text>
            </TouchableOpacity>
        )
    }
    function RemoveWalletButton(){
        const[open,setOpen] = useState()
        return(
            <TouchableOpacity
              style ={styles.button}
              onPress = {
                ()=>{ wallet.amount==0 ? showConfirmDialog() : alert("You can't delete a wallet holding coins sell/trade them first")}
              }
            >
            <Text>removeWallet</Text>
            </TouchableOpacity>
        )
    }


    const showConfirmDialog = () => {
        return Alert.alert(
          "Are your sure?",
          "Are you sure you want to remove this wallet?",
          [
            {
              text: "Yes",
              onPress: () => {
                storage.removeWallet(wallet.key)
                navigation.goBack()
              },
            },
            {
              text: "No",
            },
          ]
        );
      };


    return(
      <View style={styles.topView}>
          <View style={styles.box} alignItems='center'>
              <Text style={styles.text}>
                  Amount owned: {wallet.amount} {wallet.key}
              </Text>
              <ChartComponent purpose={'home'}/>
              <Button title='Buy' buy={true}/>
              <Button title='Sell' buy={false}/>
              {/*<Button title='Trade for other crypto currency' buy={false}/>*/}
              <RemoveWalletButton/>
          </View>
        </View>
    );


}