import { Text, View, Switch, StyleSheet, TouchableOpacity,FlatList, Image} from 'react-native';
import currencyData from '../config/currencyData';
import WalletComponent from '../components/WalletComponent';
import { configureWalletsScreenStyles as styles } from '../config/styles';
import Storage from '../utils/storage';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useFocusEffect } from '@react-navigation/native';

const storage = new Storage();

export default function ConfigureWalletsScreen({route,navigation}){
    
    const {wallets} = route.params
    const [data, setData] = React.useState([])
    
    useFocusEffect( //update when screen is focused
    React.useCallback(() => {
        var data = Object.keys(currencyData)
        
        if(wallets)
            wallets.forEach(w=> data.splice(data.indexOf(w.key),1))
        data.splice(data.indexOf(currencyData.EUR.key),1)
        setData(data)
    }, []))
   
    return(
        
        <View>
            {data.length!=0 ? 
            <FlatList 
                data={data}
                renderItem={(item) => {
                    const key = item.item
                    return(
                    <View style={styles.box}>
                        <View style={styles.viewLeft}>
                            <Image style={styles.coinImage} source={{uri:currencyData[key].icon}} />
                        </View>
                        <View style={styles.viewCenter}>                       
                            <Text style={styles.title}>{currencyData[key].name}</Text>
                            <Text style={styles.text}>{key}</Text>
                            
                        </View>
                        <View style={styles.btnTouchableOpacity }>
                             <Ionicons name={'add-circle-outline'} size={styles.plusIcon.size} onPress={()=> { 
                                alert("wallet created")
                                storage.createWallet(key) }} />
                        </View>
                        
                    </View>
                    )
                }}
                keyExtractor={(item) => item}
            /> : <View alignItems='center'><Text style={styles.text}>Already activated all supported wallets!</Text></View>}
            
        </View>
    )
}