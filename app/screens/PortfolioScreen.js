import React ,{useState}from 'react';
import { Text, View, Switch, StyleSheet, TouchableOpacity,FlatList,Button,ActivityIndicator} from 'react-native';
import colors from '../config/colors';
import Storage from '../utils/storage';
import { useFocusEffect } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import WalletComponent from '../components/WalletComponent';
import ChartComponent from '../components/ChartComponent'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { portfolioScreenStyles as styles } from '../config/styles';
import currencyData from '../config/currencyData';

const configureWalletName = "Configure Wallets";
const HomeStack = createNativeStackNavigator();


export default function PortfolioScreen({ navigation }){
    const storage = new Storage();
    const [wallets, setWallets] = useState();
   

    useFocusEffect( //update when screen is focused
       
        React.useCallback(() => {
            loadingHeader();
            
            storage.loadWallets().then(newWallets=> {
                setWallets(newWallets);
            })

        }, []));
    
    React.useEffect(()=>{
            doneHeader()
    },[wallets]);

    function doneHeader(){
        navigation.setOptions({
            headerRight: () => (
                <Ionicons name='checkmark-done-circle-outline' size={styles.checkmarkDone.size}></Ionicons>
            ),
          });
    }
    function loadingHeader(){
        navigation.setOptions({
            headerRight: () => (
                    <ActivityIndicator size={styles.loadingIndicator.size}/>
                ),
          });
    }
 function AddWalletButton(){
    return(
        <View style={styles.plusIconView}>
            <Ionicons name={'add-circle-outline'} size={styles.plusIcon.size} onPress={()=> navigation.navigate(configureWalletName,{wallets:wallets}) }/>
            {wallets==null ? <Text style={styles.btnText}>Click here to add a Wallet</Text>:<></>}
        </View>
    )}
 
    return(
        
            <View style={styles.viewContainer}>
             <FlatList style ={ styles.list }
                data={wallets}
                renderItem={(wallet) => 
                <TouchableOpacity >               
                    <WalletComponent wallet={wallet.item}/>
                </TouchableOpacity>
 
            
                }
                keyExtractor={(wallet) => wallet.key}
                ListFooterComponent={() => AddWalletButton()}
                onEndReached={doneHeader}
            />
             
        </View>
    );
}