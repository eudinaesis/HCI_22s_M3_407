import { StyleSheet } from 'react-native';
import colors from './colors';




export const walletScreenStyles = StyleSheet.create({
  topView:{
    flex:1,
    backgroundColor:colors.white,
    alignItems:'baseline'
  },
  box: {
    margin: 10
  },
  text: {
    fontSize: 20
  },
  button: {
    backgroundColor: colors.secondary,
    padding: 10,
    borderRadius: 20,
    marginTop: 10,
    borderWidth:1
  },
});
export const SettingsStyles=StyleSheet.create({
  topView:{
      alignItems:'stretch',
      //marginTop:20,
      backgroundColor:colors.white,
      flex:1
  },
  button:{
      padding:10,
      backgroundColor:colors.secondary,
      borderRadius:10,
      borderWidth:1,
      margin:10
  },
  buttonTxt:{
      fontSize:20,
      alignSelf: 'center'
  }
})
export const ConfigurePaymentStyles=StyleSheet.create({
  topView:{
      alignItems:'center',
      marginTop:20,
  },
  button:{
      padding:10,
      backgroundColor:colors.secondary,
      borderRadius:10,
      borderWidth:1,
      margin:10
  },
  buttonTxt:{
      fontSize:20,
      alignSelf: 'center'
  }
})
export const tradingHistoryComponentStyles = StyleSheet.create({
  box: {
      backgroundColor: colors.white,
      height: 50,
      marginTop: 5,
      marginLeft: 5,
      marginRight: 5,
      flexDirection: "row",
  },
  viewDate: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.3
  },
  textDate: {
    color: "grey",
  },
  viewIcon: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.15
  },
  coinImage: {
    height: 40,
    width: 40
  },
  viewCurrencyName: {
    flex: 0.3,
    justifyContent: "center",
    alignItems: "flex-start"
  },
  title: {
      fontWeight: 'bold',
      fontSize: 20,
  },
  viewAmount: {
    flex: 0.4,
    justifyContent: "center",
    alignItems: "flex-start"
  },
  textIncoming: {
    color: "red"
  },
  textOutgoing: {
    color: "green"
  }
});

export const configureWalletsScreenStyles = StyleSheet.create({
  plusIcon: {
    size: 50
  },
  box: {
      backgroundColor: colors.white,
      height: 50,
      marginTop: 5,
      marginLeft: 10,
      marginRight: 10,
      flexDirection: "row",
  },
  viewLeft: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.3
  },
  coinImage: {
    height: 40,
    width: 40
  },
  viewCenter: {
    flex: 0.3,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  btnTouchableOpacity: {
      height: 50,
      width: 50,
      alignItems: "flex-end",
      justifyContent: "center",
  },
  title: {
      fontWeight: 'bold',
      fontSize: 20,
  },
  text :{
      fontSize: 16
  }
});

export const mainContainerStyles = StyleSheet.create({
  tabBarActiveTintColor: colors.grey,
  tabBarInactiveTintColor: "grey",
  tabBarLabelStyle:{
      "paddingBottom": 5,
      "fontSize": 10
  },
  tabBarStyle:[
      {
          "display": "flex"
      },
      null
  ]
}); 

export const portfolioScreenStyles = StyleSheet.create({
  checkmarkDone: {
    size: 35
  },
  loadingIndicator: {
    size: "large"
  },
  plusIcon: {
    size: 50
  },
  viewContainer: {
    backgroundColor:colors.white,
    flex: 1
  },
  list: {
    height: 100
  },
  plusIconView: {
      flex: 1,
      alignItems: 'center'
    },
  btnText: {
      height: 50,
      flex: 1,
      fontSize: 25,
      padding: 3,
      fontWeight: "bold",
    }
});

export const homeScreenStyles = StyleSheet.create({
  viewContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 2,
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
    alignSelf:'center'
  },
  subtitle: {
      marginTop: 10,
      color: '#646464',
      fontSize: 16,
    },
    viewSetting:{
      alignItems:'flex-start',
      marginTop:25,
      marginLeft:10,
      //flex:1,
      //flexDirection:'row'
    },
    viewTop:{
      flex:1,
      backgroundColor: colors.white
    },
    firsTimeTxt:{
      //alignSelf:'center',
      fontSize:15,
      fontWeight:'bold'
    }
});

export const walletComponentStyles = StyleSheet.create({
  viewIcon: {
    justifyContent: 'center',
    alignItems:'center',
    flex: 0.2
  },
  viewImage: {
    height: 40,
    width: 40
  },
  viewName: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.2
  },
  box:{
    marginTop:10,
    marginLeft:5,
    marginRight:5,
    backgroundColor: colors.secondary,
    borderRadius :10,
    borderWidth:1
  },
  outerView: {
    backgroundColor: colors.offWhite,
    height: 80,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    flexDirection: "row",
    borderRadius:20
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
  },
 
  subtitle: {
    color: "grey"
  },
  viewAmount: {
    flex: 0.3,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    flexDirection:'row',
    marginLeft:20,
    marginRight:20
  }, 
  txtAmount: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'stretch',
  },
  viewChart:{
    flex:0.3,
    justifyContent: 'center', 
    alignItems:'flex-start',
  }
});

export const tradingStyles = StyleSheet.create({
  dropdown:{
    marginLeft: 12,
    borderWidth: 1,
    padding: 5,
    contentContainer:{
        alignItems: 'center',
    }
},beginnerHelpMessage:{
    fontSize:20 ,
    alignSelf:'center' ,
    marginTop:50,
},
topContainer:{
    flexDirection:'column',
    marginTop: 50,
    margin:20,
    alignItems: 'flex-start',
    height :'100%',
    width : '100%',
    backgroundColor:colors.white
},
innerView:{
    flexDirection:'row',
    marginTop: 50,
    margin:20,
    alignItems: 'flex-start',
   
},
backgroundView:{
  backgroundColor:colors.white,
  flex:1
},

text:{
    fontSize: 25,
    fontWeight: "bold",
    
},
DropdownButton: {
    borderRadius: 10,
    height: 30,
    width: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.secondary,
    marginLeft: 15,
    borderWidth:1
  },
  button: {
    borderWidth:1,
    borderRadius: 10,
    height: 50,
    width: 80,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor:colors.secondary,
  },
btnText: {
    height: 50,
    flex: 1,
    fontSize: 25,
    padding: 3,
    fontWeight: "bold",
  },
swapButton:{
    borderRadius: 100,
    height: 40,
    width: 40,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.secondary,
    borderWidth:1
},
selectionPartText:{
    fontSize:20,
},
icons:{
    height:20,
    width:20,
}
});


export const tradingConfirmationStyles = StyleSheet.create({
  topView:{
      alignItems:'center',
      backgroundColor: colors.white,
      flex:1
      
  },
  scrollView:{
    contentContainer:{
      flex:1,
      backgroundColor: colors.white,
    },
    },
    
  introView:{
      flexDirection :'row',
      alignContent: 'center'
  },
  networkFeeBox:{
      flexDirection:'row',
  },
  tradeButton:{
      margin:30,
      padding:10,
      borderRadius:10,
      borderWidth:1,
      backgroundColor:colors.secondary
  },
  networkFeeButton:{
      borderRadius: 10,
      height: 50,
      width: 80,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: colors.secondary,
      margin:10,
      borderWidth: 1
        
  },
  text:{
      fontSize :20,
      margin: 20
  },
  textInput:{
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      
  },
  rowView:{
      flexDirection:'row',
      alignItems: 'center',
      borderRadius:10,
      backgroundColor:colors.white,
  }
})