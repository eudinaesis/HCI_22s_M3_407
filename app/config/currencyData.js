import { Image } from 'react-native';
import btcLogo from '../assets/bitcoin_logo.png'
import ethLogo from '../assets/ethereum_logo.png'
import solLogo from '../assets/solana-logo.png'
import usdtLogo from '../assets/tether_logo.png'
import eurIcon from '../assets/eur_icon.png'
export default Currencies={ 
    BTC : {
        key : 'BTC',
        name: 'Bitcoin',
        icon : Image.resolveAssetSource(btcLogo).uri,
        subUnitName : 'Satoshi',
        conversionNecessary: (btcAmount)=>{return btcAmount<0.0001},
        subunitConversion: (btcAmount)=>{return btcAmount/0.00000001},
        symbol : '₿',
    },
    ETH : {
        key : 'ETH',
        name: 'Ethereum',
        icon : Image.resolveAssetSource(ethLogo).uri,
        subUnitName: 'Gwei',
        conversionNecessary: (ethAmount)=>{return ethAmount<0.0001},
        subunitConversion: (ethAmount)=>{return ethAmount/0.000000001},
        symbol: 'Ξ',

    },
    USDT : {
        key:'USDT',
        name:'Tether',
        icon :  Image.resolveAssetSource(usdtLogo).uri,
        conversionNecessary: (btcAmount)=>{return false},
        symbol: '₮'

    },
    SOL:{
        key:'SOL',
        name:'Solana',
        icon: Image.resolveAssetSource(solLogo).uri,
        conversionNecessary: (btcAmount)=>{return false},
        symbol: '◎'
    },
    EUR:{
        key:'EUR',
        name:'Euro',
        icon: Image.resolveAssetSource(eurIcon).uri,
        symbol: '€'
    },
}

