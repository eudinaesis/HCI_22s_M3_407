export default {
    primary: '#373737',
    secondary: '#8BF1C3',
    tertiary:'#E23E4A',
    subColor1:'#40A686',
    black: '#000',
    white: '#FFFFFF',
    grey : 'grey'
}