import React, { Component } from 'react';
import { View, Image, ScrollView, Text, StyleSheet } from 'react-native';
import CryptoIcon from 'react-native-crypto-icons';
import currencyData from '../config/currencyData.js';
import { tradingHistoryComponentStyles as styles } from '../config/styles.js';

export default class TradingHistoryComponent extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
    }

    convertDate(dateString){
        var date = new Date(dateString)
        var month=date.getMonth()+1
        return date.getDate()+"."+month+"."+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes();
    }

    render(){
        return(
           <View style={styles.box}>
                <View style={styles.viewDate}>
                    <Text style={styles.textDate}>{this.convertDate(this.props.data.date)}</Text>
                </View>
                <View style={styles.viewIcon}>
                    <Image style={styles.coinImage} source={{uri:currencyData[this.props.data.key].icon}} />
                </View>
                <View style={styles.viewCurrencyName}>
                    <Text style={styles.title}>{this.props.data.key}</Text>
                </View>
                <View style={styles.viewAmount}>
                    {this.props.data.incoming 
                        ? <Text style={styles.textIncoming}>{this.props.data.amount}</Text>
                        : <Text style={styles.textOutgoing}>{this.props.data.amount}</Text>
                    }
                </View>
           </View>
        )
                
    }
}