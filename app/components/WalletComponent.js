import React, { Component, useContext } from 'react';
import { View, Image, ScrollView, Text, StyleSheet,TouchableOpacity } from 'react-native';
import CryptoIcon from 'react-native-crypto-icons';
import currencyData from '../config/currencyData.js';
import ChartComponent from './ChartComponent'
import { useNavigation } from '@react-navigation/native';
import { walletComponentStyles as styles } from '../config/styles.js';
import colors from '../config/colors.js';

const eurSymbol = '€';

import { PriceContext } from '../utils/priceContext';

export default function WalletComponent({wallet}){
    const prices = useContext(PriceContext)[0];
//    console.log(prices)
    const navigation = useNavigation();
    const key = wallet.key;
//    console.log(key)
    const price = prices[key]["currentPrice"];

    return(
        <TouchableOpacity style={styles.box}
            //style={styles.box}
            onPress = {()=>{ 
                navigation.navigate('WalletScreen', {name:currencyData[key].name+' Wallet' , wallet:wallet})
            }}
            >
            <View style={styles.outerView}>
                <View style={styles.viewIcon}>
                    <Image 
                        style={styles.viewImage}
                        source={{uri:currencyData[key].icon}} />
                </View>

                <View style={styles.viewName}>
                    <Text style={styles.title}>{key}</Text>
                    <Text style={styles.subtitle}>{currencyData[key].name}
                    
                    </Text>
                </View>

                <View style={styles.viewChart}>
                    <ChartComponent purpose ='walletComponent'/>
                </View>

                
            </View><View style={styles.viewAmount}>
                    <Text>{wallet.amount} {currencyData[key].symbol}</Text>
                    <Text style={styles.txtAmount}> {wallet.amount * price} {eurSymbol}</Text>
                </View>
        </TouchableOpacity>
   
    )
                
    
}
//<Text style={styles.title}>{currencyData[wallet.key].conversionNecessary(wallet.amount) ? currencyData[wallet.key].subunitConversion(wallet.amount): wallet.amount}</Text>
// <ChartComponent props ={{height:10,width: 20}} ></ChartComponent>