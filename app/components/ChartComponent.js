// 7 Type of Graph using React Native Chart Kit
// https://aboutreact.com/react-native-chart-kit/

// import React in our code
import React from 'react';
import colors from '../config/colors';
// import all the components we are going to use
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
} from 'react-native';

//import React Native chart Kit for different kind of Chart
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';


export default function MyLineChart  ({purpose})  { 
  const[labels,setLabels] = React.useState()
  
  
  React.useEffect(() => {
    let tempLabels = []
    for(var i=6;i >=0;i--){
        var d = new Date()
        d.setDate(d.getDate() - i);
        var month = d.getMonth()+1
        tempLabels.push(d.getDate()+"."+month)
    }
    setLabels(tempLabels)
  }, []);

 function renderHomeChart(){
      return (
          <LineChart
            data={{
              labels: labels,
              datasets: [
                {
                  data: [20, 40, 28, 80, 99, 43],
                  strokeWidth: 2,
                },
              ],
            }}
            width={Dimensions.get('window').width - 32}
            height = {200}
            chartConfig={{
              backgroundColor: colors.white,
              backgroundGradientFrom: colors.white,
              backgroundGradientTo: colors.white,
              decimalPlaces: 2,
              color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              style: {
                borderRadius: 16,
              },
            }}
            style={{
              marginVertical: 8,
              borderRadius: 16,
            }}
          />
      );

     }
     
     
     function renderWalletChart(){
      return (
        <LineChart
          data={{
            
            labels: labels,
            datasets: [
              {
                data: [20, 40, 28, 40, 28],
                strokeWidth: 2,
              },
            ],
          }}
          
          width={200}
          height = {80}
          chartConfig={{
            backgroundColor: colors.white,
            backgroundGradientFrom: colors.white,
            backgroundGradientTo: colors.white,
            decimalPlaces: 2,
            fontSize:5,
            strokeWidth:1,
            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            style: {
              borderRadius: 16,
            },
          }}
          style={{
            paddingRight:0,
            paddingLeft:0,
            padding:0,
            marginVertical: 8,
            borderRadius: 16,
          }}
        />
      
    );
     }
     const home = 'home'
     return(
       <View>
        {purpose ==='home' ? renderHomeChart() : renderWalletChart()}
       </View>
     )
     
 }
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    //padding: 10,
  },
  header: {
    textAlign: 'center',
    fontSize: 18,
    padding: 16,
    marginTop: 16,
  },
});
