import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import MainContainer from './app/MainContainer.js';




const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <MainContainer />
  );
}

//npm start
