## Readme - M3

* Gruppe:	4
* Team-Nr.: 7
* Projektthema: Kryptowallet

### Implementierung

Framework:	React Native iOS

API-Version:	iOS 14.5

Gerät(e), auf dem(denen) getestet wurde:

* iPhone 8 Simulator
* iPhone 13 mini

Externe Libraries und Frameworks:

* React Native Async Storage,
* React Navigation,
* Expo,
* Expo Secure Store,
* Expo Status Bar,
* React Dom,
* React Native Chart Kit,
* React Native Crypto Icons,
* React Native Safe Area Context,
* React Native Screens,
* React Native Vector Icons,
* Reactjs Popup


Dauer der Entwicklung:

~40 Stunden

Weitere Anmerkungen:

Verfügbar zum Testen über Expo Go unter: [https://snack.expo.dev/@pnorthup/hci_22s_m3_407](https://snack.expo.dev/@pnorthup/hci_22s_m3_407)
